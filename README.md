# gitlab-bitbar

> Note: The `BitBar` project has been renamed to [xbar](https://github.com/matryer/xbar)

`xbar` Script to show Issues / MR's / ToDo's from multiple GitLab instances in the Menu Bar

![ScreenShot](screen-shot.png)

It will rotate through each instance, and show each one in a different color.  If an instance has no Issues, MR's, or ToDo's, it will not show up in the top bar, or in the menu.

## Download

Download [gitlab.5m.rb](gitlab.5m.rb) and put it in your `xbar` scripts directory

## Requirements

- Install [xbar](https://github.com/matryer/xbar#get-started) - `brew cask install xbar`
- Install jq - `brew install jq`
- Change the top line of the `gitlab.5m.rb` script to point to your ruby location. It sometimes has trouble with the system ruby. Brew ruby seems to work better.

## Configuration

You will need a token for each instance with API  permissions only (wish there was a read only API option). Edit the server blocks below with the instance info.

```
:server   => <The DNS host name of the instance>
:name     => <The friendly name to show in the menu>
:color    => <The lowercase color name, blank switches with dark mode>
:username => <Your login user name on the instance>
:token    => <Your Personal Access Token with API permissions only>
```

Add or delete server blocks. There doesn't have to be four. More blocks means longer check times.

## Usage

Simply put this file in your `xbar` scripts directory, and run `xbar`. 

The file name must be in the format: `something.5m.rb` where 5m is your desired check time in minutes. You can make it check more or less frequently, although I wouldn't recommend going down to 1m, especially if your connection is slow or you use cell data sometimes with more than 2 or 3 server blocks. There is no locking.
