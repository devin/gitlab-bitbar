#!/usr/local/opt/ruby/bin/ruby -U
#
# BitBar Script to show Issues / MR's / ToDo's 
# from multiple GitLab instances in the Menu Bar
#
# Contact: Devin Sylva <dsylva@gitlab.com>
#
# Download: 
# https://gitlab.com/dsylva/gitlab-bitbar
#
# Requires: https://github.com/matryer/bitbar
# Requires: Change the top line to your ruby location
# Requires: brew install jq
#
# Configuration:
# You will need a token for each instance with API 
# permissions only (wish there was a read only API option).
# Edit the server blocks below with the instance info.
#
# :server   => <The DNS host name of the instance>
# :name     => <The friendly name to show in the menu>
# :color    => <The lowercase color name, blank switches with dark mode>
# :username => <Your login user name on the instance>
# :token    => <Your Personal Access Token with API permissions only>
#
# Add or delete server blocks. There doesn't have to be four.
# More blocks means longer check times.
#
# Usage:
# Simply put this file in your BitBar scripts directory,
# and run BitBar. 
#
# The file name must be in the format: <something>.5m.rb
# where 5m is your desired check time in minutes. You can
# make it check more or less frequently, although I wouldn't
# recommend going down to 1m, especially if your connection
# is slow or you use cell data sometimes with more than 2 
# or 3 server blocks. There is no locking.

# Edit these server blocks, or add more
servers = []
servers << { :server => 'gitlab.com',
						 :name => "GitLab.com",
						 :color => '',
						 :username => 'dsylva',
						 :token => "XXXXXXXXXXXXXXXXXXXX" }
servers << { :server => 'ops.gitlab.net',
						 :name => "Ops",
						 :color => 'orange',
						 :username => 'dsylva',
						 :token => "XXXXXXXXXXXXXXXXXXXX" }
servers << { :server => 'dev.gitlab.org',
						 :name => "Dev",
						 :color => 'purple',
						 :username => 'dsylva',
						 :token => "XXXXXXXXXXXXXXXXXXXX" }
servers << { :server => 'my.home.lab.instance.example.net',
						 :name => "Home",
						 :color => 'blue',
						 :username => 'devin',
						 :token => "XXXXXXXXXXXXXXXXXXXX" }


##############################################################################
# No need to edit below here
##############################################################################

# This fetches each number and prints the menubar rotating text
for s in servers
	s[:issues] = `curl -s --header "PRIVATE-TOKEN: #{s[:token]}" 'https://#{s[:server]}/api/v4/issues?state=opened&scope=assigned_to_me&per_page=100' | /usr/local/bin/jq length`.chomp
	s[:merges] = `curl -s --header "PRIVATE-TOKEN: #{s[:token]}" 'https://#{s[:server]}/api/v4/merge_requests?state=opened&scope=assigned_to_me&per_page=100' | /usr/local/bin/jq length`.chomp
	s[:todos] = `curl -s --header "PRIVATE-TOKEN: #{s[:token]}" https://#{s[:server]}/api/v4/todos?per_page=100 | /usr/local/bin/jq length`.chomp
	if s[:issues].to_i > 0 or s[:todos].to_i > 0
		color = "color=#{s[:color]}" unless s[:color].nil? or s[:color] == ''
		puts "#{s[:issues]}/#{s[:merges]}/#{s[:todos]} | dropdown=false #{color}"
	end
end

# This prints the menu items for the drop down
for s in servers
	if s[:issues].to_i > 0 or s[:todos].to_i > 0
		puts "---"
		puts "#{s[:issues]} #{s[:name]} Issues | href='https://#{s[:server]}/dashboard/issues?assignee_username=#{s[:username]}'"
		puts "#{s[:merges]} #{s[:name]} Merge Requests | href='https://#{s[:server]}/dashboard/merge_requests?assignee_username=#{s[:username]}'"
		puts "#{s[:todos]} #{s[:name]} ToDos | href='https://#{s[:server]}/dashboard/todos'"
	end
end
